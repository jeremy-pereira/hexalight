//
//  Model.swift
//  Hexalight
//
//  Created by Jeremy Pereira on 22/09/2021.
//

import Foundation
import Combine
import Toolbox

final class Model: ObservableObject
{
	struct Cell
	{
		enum State
		{
			case current
			case visited
			case unvisited
		}

		let resistance: Int
		var state: State = .unvisited

		init(resistance: Int)
		{
			self.resistance = resistance
		}
	}

	struct SearchPoint: Comparable
	{

		let coordinate: Coordinate
		let pathLength: Int


		/// Compares to search points
		///
		/// A search point is considered to be less than another search point if
		/// it has a shorter path length or if the lengths are the same, it is
		/// lower in the grid i.e. its coordinate is less.
		/// - Returns: true if the left search point is less than the right one.
		static func < (lhs: Model.SearchPoint, rhs: Model.SearchPoint) -> Bool
		{
			return lhs.pathLength < rhs.pathLength
				|| (lhs.pathLength == rhs.pathLength && lhs.coordinate < rhs.coordinate)
		}
	}

	@Published var grid: HexaGrid<Cell>

	private var searchPoints: OrderedArray<SearchPoint, SearchPoint> = []

	static let resistanceRange = 0 ... 256

	var cellsPerIteration = 20
	let rows: Int
	let width: Int

	init(rectangleRows: Int, width: Int)
	{
		self.rows = rectangleRows
		self.width = width

		grid = HexaGrid(rectangleRows: rectangleRows, width: width)
		{
			Cell(resistance: Model.resistanceRange.randomElement()!)
		}
	}

	func nextSearchStep()
	{
		guard !grid.isEmpty else { return }

		if searchPoints.isEmpty
		{
			// Assume we haven't started, so we start from scratch
			resetGrid()
		}
		else
		{
			var cellCount = 0
			while let thisPoint = searchPoints.first, cellCount < cellsPerIteration
			{
				searchPoints.remove(at: 0)
				grid[thisPoint.coordinate].state = .visited
				let adjacents = grid.coordinatesNext(to: thisPoint.coordinate)
					.filter { grid[$0].state == .unvisited }
				for j in adjacents
				{
					let newPoint = SearchPoint(coordinate: j,
											   pathLength: thisPoint.pathLength + grid[j].resistance)
					searchPoints.insert(newPoint)
					grid[j].state = .current
				}
				cellCount += 1
			}
		}
	}

	private func resetGrid()
	{
		grid = HexaGrid(rectangleRows: rows, width: width)
		{
			Cell(resistance: Model.resistanceRange.randomElement()!)
		}
		// Pick a cell on the top row from which to start
		let y = grid.endY - 1
		let xRange = grid.xRange(row: y)
		let x = xRange.lowerBound + (xRange.upperBound - xRange.lowerBound) / 2
		let startCoodinate = Coordinate(x, y)
		searchPoints.insert(SearchPoint(coordinate: startCoodinate, pathLength: grid[startCoodinate].resistance))
		grid[startCoodinate].state = .current
	}
}
