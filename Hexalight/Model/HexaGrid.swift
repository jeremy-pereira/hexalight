//
//  HexaGrid.swift
//  Hexalight
//
//  Created by Jeremy Pereira on 17/09/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.


/// A hexagonal grid
///
/// This uses an axial coordinate system which means that the x-axis is a
/// straight line of hexagons and the y axis is a line at 60 degrees to the
/// x-axis
/// ```
///         y
///        /
///       /
///  \ / \ / \ / \ /
///   |   |   |   |
///  / \ / \ / \ / \
/// | O |   |   |   |  --> x
/// ```
///
struct HexaGrid<Cell>
{
	private struct Row
	{

		var startIndex: Int { xOffset + cells.startIndex }

		var endIndex: Int { xOffset + cells.endIndex }

		let xOffset: Int
		private var cells: [Cell] = []

		init(xOffset: Int, width: Int, emptyCell: () -> Cell)
		{
			guard width > 0 else { fatalError("Hexagrid can't yet support empty rows") }
			self.xOffset = xOffset
			for _ in 0 ..< width
			{
				cells.append(emptyCell())
			}
		}

		func contains(x: Int) -> Bool
		{
			return (xOffset ..< (cells.count + xOffset)).contains(x)
		}

		subscript(x: Int) -> Cell
		{
			get
			{
				cells[x - xOffset]
			}
			set
			{
				cells[x - xOffset] = newValue
			}
		}
	}

	private var yOffset: Int

	private var rows: [Row] = []


	/// Initialise a hexagrid in the shape of a rectangle
	///
	/// 0, 0 of the grid is assumed to be in one corner of the rectangle with the
	/// actual corner in its middle. Every even row is displaced by an extra -1
	/// to fill the rectangle
	///
	/// - Parameters:
	///   - rectangleRows: Number of rows in the rectangle
	///   - width: width of the grid
	///   - emptyCell: function thaat returns an empty cell that will be used to
	////               initialise the cells
	init(rectangleRows: Int, width: Int, emptyCell: () -> Cell)
	{
		yOffset = 0
		for i in 0 ..< rectangleRows
		{
			rows.append(Row(xOffset: -(i / 2), width: width, emptyCell: emptyCell))
		}
	}

	init()
	{
		yOffset = 0
	}

	func contains(coordinate: Coordinate) -> Bool
	{
		guard (yOffset ..< (yOffset + rows.count)).contains(coordinate.y)
		else { return false }
		return rows[coordinate.y - yOffset].contains(x: coordinate.x)
	}

	subscript(coordinate: Coordinate) -> Cell
	{
		get
		{
			let rowNum = coordinate.y - yOffset
			return rows[rowNum][coordinate.x]
		}
		set
		{
			let rowNum = coordinate.y - yOffset
			rows[rowNum][coordinate.x] = newValue
		}
	}

	func coordinatesNext(to coordinate: Coordinate) -> [Coordinate]
	{
		return Vector.adjacents.map { coordinate + $0 }.filter { self.contains(coordinate: $0) }
	}

	var rectangularStartX: Int
	{
		guard !rows.isEmpty else { return 0 }

		let ret = rows.enumerated().reduce(Int.max)
		{
			let currentMin = $0
			let (i, row) = $1
			let projectedX = Coordinate(row.startIndex, i + yOffset).xAxisProjection
			return projectedX < currentMin ? projectedX : currentMin
		}
		return ret
	}


	var rectangularEndX: Int
	{
		guard !rows.isEmpty else { return 0 }

		let ret = rows.enumerated().reduce(Int.min)
		{
			let currentMax = $0
			let (i, row) = $1
			let projectedX = Coordinate(row.endIndex, i + yOffset).xAxisProjection
			return projectedX > currentMax ? projectedX : currentMax
		}
		return ret
	}

	var endY: Int { yOffset + rows.count }
	var startY: Int { yOffset }
	var rectangularStartY: Int { startY }
	var rectangularEndY: Int { endY }

	func xRange(row: Int) -> Range<Int>
	{
		self.rows[row].startIndex ..< self.rows[row].endIndex
	}

}

extension HexaGrid: MutableCollection, BidirectionalCollection
{
	var startIndex: Coordinate
	{
		let xStart = rows.first?.xOffset ?? 0
		return Coordinate(xStart, self.yOffset)
	}

	var endIndex: Coordinate
	{
		let yEnd = yOffset + rows.count
		return Coordinate(0, yEnd)
	}

	func index(before i: Coordinate) -> Coordinate
	{
		guard (self.contains(coordinate: i) || i == self.endIndex) && i != self.startIndex
		else { fatalError("Coordinate \(i) is not in the grid or is startIndex") }

		let ret: Coordinate
		let rowNum = i.y - yOffset
		if i == self.endIndex
		{
			let newRowNum = i.y - yOffset - 1
			ret = Coordinate(rows[newRowNum].endIndex - 1, i.y - 1)
		}
		else if i.x > rows[rowNum].xOffset
		{
			ret = Coordinate(i.x - 1, i.y)
		}
		else
		{
			ret = Coordinate(rows[rowNum - 1].endIndex - 1, i.y - 1)
		}
		return ret
	}

	func index(after i: Coordinate) -> Coordinate
	{
		guard self.contains(coordinate: i)
		else { fatalError("Coordinate \(i) is not in the grid") }

		let rowNum = i.y - yOffset
		let ret: Coordinate
		if rows[rowNum].contains(x: i.x + 1)
		{
			ret = Coordinate(i.x + 1, i.y)
		}
		else if rowNum + 1 < rows.count
		{
			ret = Coordinate(rows[rowNum + 1].xOffset, i.y + 1)
		}
		else
		{
			ret = endIndex
		}
		return ret
	}
}

/// A coordinate within the grid
///
/// Coordinates are axial, which mrans the x axis is on a row and the t
/// axis is on one of the two 60 degree diagonals. In our case, the one
/// that makes an accute angle with the x axis
struct Coordinate: Comparable, CustomStringConvertible
{

	/// Compare two coordinates
	///
	/// `Coordinate` needs to be comparable so we can use it as an index for
	/// `HexaGrid`. We use simple row major order i.e. `y` is more important than
	/// `x`
	/// - Parameters:
	///   - lhs: First coordinate to comapre
	///   - rhs: second coordinate to compare
	/// - Returns: `true` if `lhs` is less than `rhs`
	static func < (lhs: Coordinate, rhs: Coordinate) -> Bool
	{
		return lhs.y < rhs.y || (lhs.y == rhs.y && lhs.x < rhs.x)
	}
	/// x coordinate
	let x: Int
	/// y coordinate
	let y: Int
	/// The projection of this coordinate onto the x axis.
	///
	/// This is calculated at initialisation because it will probably be used
	/// a lot.
	let xAxisProjection: Int

	init(_ x: Int, _ y: Int)
	{
		self.x = x
		self.y = y
		self.xAxisProjection = Coordinate.project(x: x, fromY: y, toY: 0)
	}

	/// Check id this coordinate is in a partiucular grid
	/// - Parameter grid: The `Hexagrid` to see if we are in
	/// - Returns: true if this coordinate references a cell in the given grid
	func isIn<Cell>(grid: HexaGrid<Cell>) -> Bool
	{
		return grid.contains(coordinate: self)
	}


	/// Project the x vertically to the given row
	///
	/// This is fairly complicated. Consider the grid below
	/// ```
	/// o o o o o
	///  o b o o o
	/// o a o o o
	/// ```
	/// If we are projecting `a` onto the top row, there is only one choice and
	/// we will subtract 1 from its `x` coordinate. If we are projecting `b`
	/// on to the top row, there are two choices. If `b` is on an even row, we
	/// go up and right. If `b` is on an odd row, we go up and left.
	///
	/// Similarly going downwards. If `b` is on an even row, we need to add 1. If
	/// `b` is on an odd row, we add 0.
	///
	/// - Parameter y: The y of the row to project to
	/// - Returns: The x coordinate of this cell projected to the given y
	func xProjectedTo(y: Int) -> Int
	{
		return Coordinate.project(x: self.x, fromY: self.y, toY: y)
	}


	/// Project the x vertically from a row to another row
	///
	/// This is fairly complicated. Consider the grid below
	/// ```
	/// o o o o o
	///  o b o o o
	/// o a o o o
	/// ```
	/// If we are projecting `a` onto the top row, there is only one choice and
	/// we will subtract 1 from its `x` coordinate. If we are projecting `b`
	/// on to the top row, there are two choices. If `b` is on an even row, we
	/// go up and right. If `b` is on an odd row, we go up and left.
	///
	/// Similarly going downwards. If `b` is on an even row, we need to add 1. If
	/// `b` is on an odd row, we add 0.
	/// - Parameters:
	///   - x: The x to project
	///   - fromY: The row on which the x is an x coordinate
	///   - toY: The row to go to
	/// - Returns: The x of the cell that is verttically above or below the x
	static func project(x: Int, fromY: Int, toY: Int) -> Int
	{
		let yGap = toY - fromY
		let evenXDifference = -(yGap / 2)
		var oddAdjustment = 0
		if !yGap.isMultiple(of: 2)
		{
			if !fromY.isMultiple(of: 2) && yGap > 0
			{
				oddAdjustment = -1
			}
			else if fromY.isMultiple(of: 2) && yGap < 0
			{
				oddAdjustment = 1
			}
		}
		return x + evenXDifference + oddAdjustment
	}

	var description: String { "(\(x), \(y))" }
}

/// A hexagrid coordinate vector
struct Vector
{
	let x: Int
	let y: Int

	init(_ x: Int, _ y: Int)
	{
		self.x = x
		self.y = y
	}

	static let adjacents = [(0, -1), (1, -1), (-1, 0), (1, 0), (-1, 1), (0, 1)].asVectors()
}

extension Collection where Element == (Int, Int)
{
	func asCoordinates() -> [Coordinate]
	{
		self.map { Coordinate($0.0, $0.1) }
	}
	func asVectors() -> [Vector]
	{
		self.map { Vector($0.0, $0.1) }
	}
}

func +(lhs: Coordinate, rhs: Vector) -> Coordinate
{
	Coordinate(lhs.x + rhs.x, lhs.y + rhs.y)
}
