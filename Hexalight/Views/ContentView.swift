//
//  ContentView.swift
//  Hexalight
//
//  Created by Jeremy Pereira on 17/09/2021.
//

import SwiftUI

struct ContentView: View
{
	@EnvironmentObject var model: Model

	let timer = Timer.publish(every: 0.1, on: .main, in: .common).autoconnect()

    var body: some View
	{
		VStack
		{
			HexGridContainer()
		}
		.toolbar
		{
			Button(action: singleStep)
			{
				Text("Step")
			}
		}
		.onReceive(timer)
		{
			_ in singleStep()
		}
    }

	private func singleStep()
	{
		model.nextSearchStep()
	}
}

struct ContentView_Previews: PreviewProvider
{
    static var previews: some View
	{
		ContentView().environmentObject(Model(rectangleRows: 5, width: 4))
    }
}
