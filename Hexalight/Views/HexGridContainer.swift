//
//  HexGridContainer.swift
//  Hexalight
//
//  Created by Jeremy Pereira on 22/09/2021.
//

import SwiftUI

struct HexGridContainer: NSViewRepresentable
{
	@EnvironmentObject var model: Model

	func makeNSView(context: Context) -> HexaGridNSView
	{
		let view = HexaGridNSView()
		view.dataSource = model
		return view
	}

	func updateNSView(_ nsView: HexaGridNSView, context: Context)
	{
		nsView.needsDisplay = true
	}
}

extension Model: HexagridDataSource
{
	func colour(x: Int, y: Int) -> NSColor?
	{
		let axialX = Coordinate(x, 0).xProjectedTo(y: y)
		let gridLocation = Coordinate(axialX, y)
		guard gridLocation.isIn(grid: grid) else { return nil }

		let ret: NSColor
		switch grid[gridLocation].state
		{
		case .current:
			ret = NSColor.white
		case .visited:
			ret = NSColor(red: 0, green: calculateComponent(resistance: grid[gridLocation].resistance), blue: 0, alpha: 1)
		case .unvisited:
			ret = NSColor(red: 0, green: 0, blue: calculateComponent(resistance: grid[gridLocation].resistance), alpha: 1)
		}
		return ret
	}

	var startX: Int { self.grid.rectangularStartX }

	var endX: Int { self.grid.rectangularEndX }

	var startY: Int { self.grid.rectangularStartY }

	var endY: Int { self.grid.rectangularEndY }

	private func calculateComponent(resistance: Int) -> CGFloat
	{
		let valueRange = Model.resistanceRange
		let clampedR: Int
		if resistance < valueRange.lowerBound
		{
			clampedR = valueRange.lowerBound
		}
		else if resistance > valueRange.upperBound
		{
			clampedR = valueRange.upperBound
		}
		else
		{
			clampedR = resistance
		}
		let rNormalised = valueRange.upperBound - clampedR
		return CGFloat(rNormalised) / CGFloat(valueRange.count)
	}

}
