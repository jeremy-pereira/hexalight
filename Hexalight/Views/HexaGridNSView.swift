//
//  HexaGridNSView.swift
//  Hexalight
//
//  Created by Jeremy Pereira on 22/09/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Cocoa


/// Provides an API for the view to get values from a hex grid
///
/// Coordinates are given in rectangular even-odd form. It is the responsibility
/// of the data source to convert to the axial form, if used.
protocol HexagridDataSource: AnyObject
{
	/// The x coordinate of the left most cell when projected onto the x axis
	///
	/// The axial coordinate system skews x cooordinates. Cells above the x-axis
	/// effectivelyt move one further right for each two rows.
	var startX: Int { get }
	/// The x coordinate of the right most cell when projected onto the x axis plus one
	///
	/// The axial coordinate system skews x cooordinates. Cells above the x-axis
	/// effectivelyt move one further right for each two rows and below cells move
	/// one further left for each two rows.
	var endX: Int { get }
	/// The lowest numbered row in the grid
	var startY: Int { get }
	/// One more than the highest numbered row in the grid
	var endY: Int { get }

	/// Get the value of the hexagon at the given row and column
	///
	/// x and y are given in rectangular coordinates
	/// - Parameters:
	///   - x: column of cell
	///   - y: row ofd cell
	/// - Returns: An `Int` value for the cell at the given coordinates or `nil`
	///   if there is no cell at the location
	func colour(x: Int, y: Int) -> NSColor?
}

/// A view that draws a hexagonal grid
class HexaGridNSView: NSView
{
	var dataSource: HexagridDataSource?

    override func draw(_ dirtyRect: NSRect)
	{
        super.draw(dirtyRect)
		NSColor.white.set()
		dirtyRect.fill()
		NSColor.blue.set()
		NSBezierPath.stroke(dirtyRect)
		guard let dataSource = dataSource else { return }

		let hexagonsAcross = dataSource.endX - dataSource.startX
		let hexagonRows = dataSource.endY - dataSource.startY
		// Now find out how big we can make the hexagons. The aspect ratio of
		// the view determines whether the height of the grid or the width
		// of the grid matters
		let potentialHeight = self.bounds.height / (CGFloat(hexagonRows) + 1/3)
		let widthIfHeightUsed = (potentialHeight / 1.5) * cos(CGFloat.pi / 6) * 2
		let potentialWidth: CGFloat = self.bounds.width / (CGFloat(hexagonsAcross) + 0.5)

		let hexagonWidth = widthIfHeightUsed < potentialWidth ? widthIfHeightUsed : potentialWidth

		NSColor.black.set()

		// Distance from a hexagon's centre to the midpoint of one of its sides
		let shortRadius = hexagonWidth / 2
		// Distance from a hexagon's centre to one of its vertices
		let longRadius = shortRadius / cos(CGFloat.pi / 6)
		// Verticlal ditance between centres of hexagons in adjacent rows
		let yRadius = longRadius * 1.5
		// Offset to add to hexagon centre to make sure the bottom left hexagon is
		// entirely visible
		let centreOffset = NSPoint(x: shortRadius, y: longRadius)

		for y in 0 ..< hexagonRows
		{
			for x in 0 ..< hexagonsAcross
			{
				if let colour = dataSource.colour(x: x, y: y)
				{
					colour.set()
					let centre = NSPoint(x: CGFloat(x) * shortRadius * 2 + (y.isMultiple(of: 2) ? 0 : shortRadius),
										 y: yRadius * CGFloat(y)) + centreOffset

					let path = NSBezierPath()
					path.drawHex(segmentLength: longRadius, centre: centre)
					path.fill()
				}
			}
		}

    }

	override var isOpaque: Bool { true }
}

extension NSBezierPath
{
	static let unitVertices = [
		NSPoint(x: 0, y: 1),
		NSPoint(x: cos(5 * Double.pi / 6), y: sin(5 * Double.pi / 6)),
		NSPoint(x: cos(-5 * Double.pi / 6), y: sin(-5 * Double.pi / 6)),
		NSPoint(x: 0, y: -1),
		NSPoint(x: cos(-Double.pi / 6), y: sin(-Double.pi / 6)),
		NSPoint(x: cos(Double.pi / 6), y: sin(Double.pi / 6)),
	]

	func drawHex(segmentLength: CGFloat, centre: NSPoint)
	{
		let vertices = NSBezierPath.unitVertices.map
		{
			NSPoint(x: CGFloat($0.x) * segmentLength, y: CGFloat($0.y) * segmentLength) /* * 0.95 */ + centre
		}
		self.move(to: vertices[0])
		for vertex in vertices.dropFirst()
		{
			self.line(to: vertex)
		}
		self.line(to: vertices[0])
	}

}

extension NSPoint
{
	static func +(lhs: NSPoint, rhs: NSPoint) -> NSPoint
	{
		NSPoint(x: lhs.x + rhs.x, y: lhs.y + rhs.y)
	}

	static func * (lhs: NSPoint, rhs: CGFloat) -> NSPoint
	{
		NSPoint(x: lhs.x * rhs, y: lhs.y * rhs)
	}
}
