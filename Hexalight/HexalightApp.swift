//
//  HexalightApp.swift
//  Hexalight
//
//  Created by Jeremy Pereira on 17/09/2021.
//

import SwiftUI

@main
struct HexalightApp: App
{
    var body: some Scene
	{
        WindowGroup
		{
			ContentView().environmentObject(Model(rectangleRows: 100, width: 80))
        }
    }
}
