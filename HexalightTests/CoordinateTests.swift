//
//  CoordinateTests.swift
//  HexalightTests
//
//  Created by Jeremy Pereira on 02/10/2021.
//

import XCTest
@testable import Hexalight

class CoordinateTests: XCTestCase
{

    override func setUpWithError() throws
	{
        // Put setup code here. This method is called before the invocation of
		// each test method in the class.
    }

    override func tearDownWithError() throws
	{
        // Put teardown code here. This method is called after the invocation of
		// each test method in the class.
    }

    func testXAxisProjection() throws
	{
		let belowOdd = Coordinate(0, -1)
		let belowEven = Coordinate(0, -2)
		let aboveOdd = Coordinate(0, 1)
		let aboveEven = Coordinate(0, 2)
		XCTAssert(belowOdd.xAxisProjection == -1)
		XCTAssert(belowEven.xAxisProjection == -1)
		XCTAssert(aboveOdd.xAxisProjection == 0)
		XCTAssert(aboveEven.xAxisProjection == 1)
    }

	func testProjections()
	{
		let oddCoordinate = Coordinate(1, 1)
		let evenCoordinate = Coordinate(1, 2)

		XCTAssert(oddCoordinate.xProjectedTo(y: 2) == 0)
		XCTAssert(oddCoordinate.xProjectedTo(y: 3) == 0)
		XCTAssert(oddCoordinate.xProjectedTo(y: 0) == 1)
		XCTAssert(oddCoordinate.xProjectedTo(y: -1) == 2)

		XCTAssert(evenCoordinate.xProjectedTo(y: 3) == 1)
		XCTAssert(evenCoordinate.xProjectedTo(y: 4) == 0)
		XCTAssert(evenCoordinate.xProjectedTo(y: 1) == 2)
		XCTAssert(evenCoordinate.xProjectedTo(y: 0) == 2)
	}

}
