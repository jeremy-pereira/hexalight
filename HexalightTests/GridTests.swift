//
//  GridTests.swift
//  HexalightTests
//
//  Created by Jeremy Pereira on 17/09/2021.
//

import XCTest
@testable import Hexalight

class GridTests: XCTestCase
{

    override func setUpWithError() throws
	{
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws
	{
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testRead4x5Rectangle() throws
	{
		let grid = HexaGrid<Int>(rectangleRows: 4, width: 5, emptyCell: { 1 })
		let origin = Coordinate(0, 0)
		guard origin.isIn(grid: grid)
		else
		{
			XCTFail("The origin must be in the grid")
			return
		}
		XCTAssert(grid[origin] == 1)
		XCTAssert(Coordinate(4, 0).isIn(grid: grid))
		XCTAssert(!Coordinate(5, 0).isIn(grid: grid))
		XCTAssert(Coordinate(0, 1).isIn(grid: grid))
		XCTAssert(!Coordinate(-1, 1).isIn(grid: grid))
		XCTAssert(Coordinate(-1, 2).isIn(grid: grid))
    }

	func testWriteGrid()
	{
		// First a simple integer grid
		var intGrid = HexaGrid(rectangleRows: 4, width: 5, emptyCell: { 1 })
		let coord = Coordinate(1, 2)
		XCTAssert(intGrid[coord] == 1)
		intGrid[coord] = 2
		XCTAssert(intGrid[coord] == 2)

		// Now let's try amending a struct

		struct TestStruct
		{
			var a: Bool
			var b: Int
		}
		var structGrid = HexaGrid(rectangleRows: 3, width: 3)
		{
			TestStruct(a: true, b: 0)
		}
		XCTAssert(structGrid[coord].a)
		XCTAssert(structGrid[coord].b == 0)
		structGrid[coord].a = false
		XCTAssert(!structGrid[coord].a)
		XCTAssert(structGrid[coord].b == 0)
	}

	func testCollection()
	{
		var grid = HexaGrid<Int>(rectangleRows: 3, width: 3, emptyCell: { 0 })
		var n = 0
		for y in 0 ..< 3
		{
			let xStart = -(y / 2)
			for x in xStart ..< xStart + 3
			{
				grid[Coordinate(x, y)] = n
				n += 1
			}
		}
		XCTAssert(grid.startIndex == Coordinate(0, 0))
		XCTAssert(grid.endIndex == Coordinate(0, 3))
		XCTAssert(grid.index(after: Coordinate(2, 1)) == Coordinate(-1, 2))
		XCTAssert(grid.index(after: Coordinate(1, 1)) == Coordinate(2, 1))
		XCTAssert(grid.index(after: Coordinate(1, 2)) == grid.endIndex)
		XCTAssert(grid.index(before: Coordinate(-1, 2)) == Coordinate(2, 1), "Expected \(Coordinate(2, 1)), got \(grid.index(before: Coordinate(-1, 2)))")
		XCTAssert(grid.index(before: Coordinate(2, 1)) == Coordinate(1, 1))
		XCTAssert(grid.index(before: Coordinate(1, 1)) == Coordinate(0, 1))
		XCTAssert(grid.index(before: Coordinate(1, 0)) == grid.startIndex)
		XCTAssert(grid.index(before: grid.endIndex) == Coordinate(1, 2))
	}

	func testEmptyCollection()
	{
		let grid = HexaGrid<Int>()
		XCTAssert(grid.startIndex == grid.endIndex)
	}

	func testCoordComparison()
	{
		XCTAssert(Coordinate(0, 0) < Coordinate(0, 1))
		XCTAssert(Coordinate(0, 5) > Coordinate(1, 1))
		XCTAssert(Coordinate(0, 5) >= Coordinate(1, 1))
		XCTAssert(Coordinate(0, 5) == Coordinate(0, 5))
	}

	func testAdjacentCells()
	{
		let grid = HexaGrid<Int>(rectangleRows: 3, width: 3, emptyCell: { 0 })

		let middle = Coordinate(1, 1)

		let middleAdjacents = grid.coordinatesNext(to: middle)
		let expectedMiddleAdjacents = [(1, 0), (2, 0), (0, 1), (2, 1), (0, 2), (1, 2)].asCoordinates()
		XCTAssert(middleAdjacents  == expectedMiddleAdjacents, "Expected \(expectedMiddleAdjacents), got \(middleAdjacents)")

		let origin = Coordinate(0, 0)
		let originAdjacents = grid.coordinatesNext(to: origin)
		let expectedOriginAdjacents = [(1, 0), (0, 1)].asCoordinates()
		XCTAssert(originAdjacents  == expectedOriginAdjacents, "Expected \(expectedOriginAdjacents), got \(originAdjacents)")
	}

	func testCoordinateDescription()
	{
		XCTAssert(Coordinate(1, 2).description == "(1, 2)")
	}
}
